import pytest
from brownie import OxidiqueToken, accounts

@pytest.fixture(scope="function", autouse=True)
def isolate(fn_isolation):
    pass


@pytest.fixture(scope="module", autouse=True)
def token():
    return accounts[0].deploy(OxidiqueToken, 1000, "Test Token", 18, "TST")