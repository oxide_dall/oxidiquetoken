
import brownie

def test_balance_of(token, accounts):
    assert token.balanceOf(accounts[0]) == 1000

def test_my_balance(token, accounts):
    assert token.myBalance({'from':accounts[0]}) == 1000
