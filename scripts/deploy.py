from brownie import OxidiqueToken, accounts, network

def main():
    acct = accounts.load('owner')
    OxidiqueToken.deploy(1000000, "Oxidique Token", 8, "OXD",{'from':acct})